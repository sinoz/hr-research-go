package main

import (
	"log"
	"net/http"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/sinoz/hr-research/phonebook"
)

// Transports expose the service to the network. In this first example we utilize JSON over HTTP.
func main() {
	phonebookService := phonebook.PhoneNumberServiceImpl{}

	phonebookHandler := httptransport.NewServer(
		phonebook.MakePhoneNumberServiceEndpoint(phonebookService),
		phonebook.DecodePhoneNumberRequest,
		phonebook.EncodeResponse,
	)

	http.Handle("/get", phonebookHandler)

	// starts listening at port 8080 in a blocking fashion. only logs if there was an error
	log.Fatal(http.ListenAndServe(":8080", nil))
}
