package phonebook

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type PhoneNumberEntry struct {
	Id          string     `json:_id`
	Name        string     `json:"name"`
	Gender      string     `json:"game"`
	Email       string     `json:"email"`
	PhoneNumber string     `json:"phone"`
}

func LoadPhoneBook() map[string]PhoneNumberEntry {
	jsonData, err := ioutil.ReadFile("resources/phonebook.json")
	if err != nil {
		log.Fatal(err)
		return nil
	}

	var phonenumbers []PhoneNumberEntry
	json.Unmarshal([]byte(jsonData), &phonenumbers)

	phonebook := make(map[string]PhoneNumberEntry)

	for _, phoneNumber := range phonenumbers {
		phonebook[phoneNumber.Name] = phoneNumber
	}

	return phonebook
}