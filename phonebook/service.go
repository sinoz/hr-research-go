package phonebook

import "context"

// PhoneNumberService provides operations on strings.
type PhoneNumberService interface {
	GetByName(context.Context, string) (string, error)
}

// PhoneNumberServiceImpl is a concrete implementation of PhoneNumberService
type PhoneNumberServiceImpl struct {
	// empty
}
