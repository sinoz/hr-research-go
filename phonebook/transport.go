package phonebook

import (
	"context"
	"encoding/json"
	"github.com/go-kit/kit/endpoint"
	"errors"
	"net/http"
)

var PhoneNumbers map[string]PhoneNumberEntry

func init() {
	PhoneNumbers = LoadPhoneBook()
}

func (PhoneNumberServiceImpl) GetByName(_ context.Context, name string) (string, error) {
	if name == "" {
		return "", ErrEmpty
	}

	entry, exists := PhoneNumbers[name]
	if !exists {
		return name, Err404
	}

	return entry.PhoneNumber, nil
}

// ErrEmpty is returned when an input string is empty.
var ErrEmpty = errors.New("empty string")
var Err404 = errors.New("could not find phone number by name")

// the request from the client containing the name of whos phone number should be looked up
type PhoneNumberLookUpRequest struct {
	Name    string     `json:"Name"`
}

// the response to send back to the client potentially containing the phone number to look up
type PhoneNumberLookUpResponse struct {
	PhoneNumber       string      `json:"PhoneNumber"`
	Err               string      `json:"err,omitempty"` // errors don't define JSON marshaling
}

// Endpoints are a primary abstraction in go-kit. An endpoint represents a single RPC (method in our service interface)
func MakePhoneNumberServiceEndpoint(svc PhoneNumberService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(PhoneNumberLookUpRequest)

		phoneNumber, err := svc.GetByName(ctx, req.Name)
		if err != nil {
			return PhoneNumberLookUpResponse{phoneNumber, err.Error()}, nil
		}

		return PhoneNumberLookUpResponse{phoneNumber, ""}, nil
	}
}

func DecodePhoneNumberRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request PhoneNumberLookUpRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}